package epsi.java.coursjava.api.dto;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SuperHeroDTO {
    private Long id;
    private String superHeroName;
    private String secretIdentity;

}
