package epsi.java.coursjava.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "vilain")

public class Vilain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(
        name = "name",
        nullable = false,
        length = 100
    )
    private String name;

    @Column(
        name = "secret_identity"
    )
    private String secretIdentity;

    @OneToOne(mappedBy = "nemesis")
    private SuperHero nemesis;

    @ManyToMany(mappedBy = "vilains")
    private List<SuperHero> superHeroes;
}
