package epsi.java.coursjava.model;



import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "power")

public class Power {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(
            nullable = false
    )
    private String name;
    private String description;

    @ManyToMany(mappedBy = "powers")
    private List<SuperHero> superHeroes;
}
