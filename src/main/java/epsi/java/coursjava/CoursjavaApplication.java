package epsi.java.coursjava;

import epsi.java.coursjava.model.Power;
import epsi.java.coursjava.model.SuperHero;
import epsi.java.coursjava.repository.PowerRepository;
import epsi.java.coursjava.repository.SuperHeroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CoursjavaApplication {

	Logger logger = LoggerFactory.getLogger(CoursjavaApplication.class)

	private final PowerRepository powerRepository;
	private final SuperHeroRepository superHeroRepository;

	public CoursjavaApplication(PowerRepository powerRepository, SuperHeroRepository superHeroRepository) {
		this.powerRepository = powerRepository;
		this.superHeroRepository = superHeroRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(CoursjavaApplication.class, args);
	}


	@PostConstruct
	public void initData(){

		if(powerRepository.count() == 0){
			logger.info("New power created: {}", powerRepository.save(createPower("Vol", "Permet de voler")));
		}

		if(superHeroRepository.count() == 0){
			SuperHero superHero = new SuperHero();
			superHero.setSuperHeroName("Superman");
			superHero.setSecretIdentity("Clark Kent");

			List<Power> powerList = new ArrayList<>();
			powerRepository.findById("1").ifPresent(powerList::add);

			powerList.add(createPower("Super Force", "Permet de taper fort"));

			SuperHero createdSuperHero = superHeroRepository.save(superHero);
			createdSuperHero.setPowers(powerList);
			createdSuperHero = superHeroRepository.save(superHero);
			logger.info("New SuperHero created {}", createdSuperHero);
		}
	}

	private Power createPower(String name, String description){
		Power power = new Power();
		power.setName(name);
		power.setDescription(description);
		return power;
	}
}


