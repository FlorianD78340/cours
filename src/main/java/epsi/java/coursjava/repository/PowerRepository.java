package epsi.java.coursjava.repository;

import epsi.java.coursjava.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PowerRepository extends JpaRepository<Power, Long> {
    List<Power> findAllByName(String name);

    List<Power> findAllByNameIsStartingWith(String name);
}
