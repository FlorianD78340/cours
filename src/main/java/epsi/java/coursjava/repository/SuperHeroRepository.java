package epsi.java.coursjava.repository;

import epsi.java.coursjava.model.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {

}
