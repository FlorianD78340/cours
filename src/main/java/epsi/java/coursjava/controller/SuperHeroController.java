package epsi.java.coursjava.controller;

import epsi.java.coursjava.api.dto.SuperHeroDTO;
import epsi.java.coursjava.model.SuperHero;
import epsi.java.coursjava.repository.SuperHeroRepository;

import net.bytebuddy.implementation.bind.annotation.Super;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.rmi.ServerError;
import java.rmi.ServerException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        path = "superhero",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class SuperHeroController {

    private final SuperHeroRepository superHeroRepository;

    public SuperHeroController(SuperHeroRepository superHeroRepository) {
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAllSuperHero(){
        return ResponseEntity.ok(this.superHeroRepository
                .findAll()
                .stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList())
        );
    }

    private SuperHeroDTO mapToDTO(SuperHero superHero){

        return new SuperHeroDTO(
                superHero.getId(),
                superHero.getSuperHeroName(),
                superHero.getSecretIdentity()
        );
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHero> create(@RequestBody SuperHeroDTO newSuperHeroDTO){

        SuperHero superHero = mapToEntity(newSuperHeroDTO);

        return ResponseEntity.status(HttpStatus.CREATED).body(this.superHeroRepository.save(superHero));

    }

    private SuperHero mapToEntity(SuperHeroDTO superHeroDTO){
        SuperHero superHero = new SuperHero();
        superHero.setId(superHeroDTO.getId());
        superHero.setSuperHeroName(superHeroDTO.getSuperHeroName());
        superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
        return superHero;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> getById(@PathVariable Long id){
        return this.superHeroRepository.findById(id)
                .map(superHero -> ResponseEntity.ok(mapToDTO(superHero)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/superhero/{id}")
    public ResponseEntity<SuperHeroDTO> update(@PathVariable(value = "id") Long id,
                                                     @RequestBody SuperHeroDTO superHeroDTO){
        if(this.superHeroRepository.findById(id).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        if(id!=superHeroDTO.getId()){
            return ResponseEntity.badRequest().build();
        }

        SuperHero superHeroUpdate = mapToEntity(superHeroDTO);
        return ResponseEntity.ok(mapToDTO(this.superHeroRepository.save(superHeroUpdate)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        this.superHeroRepository.deleteById(id);
    }

}