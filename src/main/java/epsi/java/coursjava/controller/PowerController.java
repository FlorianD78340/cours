package epsi.java.coursjava.controller;

import epsi.java.coursjava.api.dto.PowerDTO;
import epsi.java.coursjava.model.Power;
import epsi.java.coursjava.repository.PowerRepository;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@RestController
@RequestMapping(
        path = "power",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class PowerController {

    private final PowerRepository powerRepository;

    public PowerController(PowerRepository powerRepository) {
        this.powerRepository = powerRepository;
    }

    @GetMapping
    public ResponseEntity<List<PowerDTO>> getAll(){
        return ResponseEntity.ok(this.powerRepository
                .findAll()
                .stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "search")
    public ResponseEntity<List<PowerDTO>> searchByName(@RequestParam(name = "name") String name){
        return ResponseEntity.ok(this.powerRepository
                .findAllByNameIsStartingWith(name)
                .stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList())
        );
    }

    private PowerDTO mapToDTO(Power power) {
        return PowerDTO.builder()
                .id( getId())
                .name(getName())
                .description(getDescription())
                .build();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Power> create(@RequestBody PowerDTO powerDTO){
        powerDTO.setID(0);
        Power power = mapToEntity(powerDTO);

        return ResponseEntity.status(HttpStatus.CREATED).body(this.powerRepository.save(power));

    }

    private Power mapToEntity(PowerDTO powerDTO) {
        Power power = new Power();
        power.setId(powerDTO.getId());
        power.setName(powerDTO.getName());
        power.setDescription(powerDTO.getDescription());

        return power;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<PowerDTO> getById(@PathVariable Long id){
        return this.powerRepository.findById(id)
                .map(power -> ResponseEntity.ok(mapToDTO(power)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/power/{id}")
    public ResponseEntity<PowerDTO> update(@PathVariable(value = "id") Long id,
                                               @RequestBody PowerDTO powerDTO){
        if(this.powerRepository.findById(id).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        if(id!=powerDTO.getId()){
            return ResponseEntity.badRequest().build();
        }

        Power power = mapToEntity(powerDTO);
        return ResponseEntity.ok(mapToDTO(this.powerRepository.save(power)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        this.powerRepository.deleteById(id);
    }
}
